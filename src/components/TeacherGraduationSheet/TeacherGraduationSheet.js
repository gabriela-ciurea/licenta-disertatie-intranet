import React from "react";
import "./TeacherGraduationSheet.css";
import StudentInfo from "../StudentInfo/StudentInfo";
import GraduationWork from "../GraduationWork/GraduationWork";
import TextField from "@material-ui/core/TextField";
import {KeyboardDatePicker, MuiPickersUtilsProvider} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import Paper from "@material-ui/core/Paper";

export default class TeacherGraduationSheet extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            graduationWork: {
                title: 'Titlu',
                treatedProblems: 'Probleme tratate',
                practicePlaceAndTime: 'Locul şi durata practicii',
                bibliography: 'Bibliografie',
                particularAspects: 'Aspecte particulare'
            },
            teacherApprovals: [
                {
                    approvalDate: new Date('2020-01-01'),
                    treatedProblems: 'Capitole si probleme analizate 1'
                },
                {
                    approvalDate: new Date('2021-01-01'),
                    treatedProblems: 'Capitole si probleme analizate 2'
                },
                {
                    approvalDate: new Date('2022-01-01'),
                    treatedProblems: 'Capitole si probleme analizate 3'
                },
                {
                    approvalDate: new Date('2023-01-01'),
                    treatedProblems: 'Capitole si probleme analizate 4'
                }
            ]
        };

        this.handleGraduationWorkFieldChange = this.handleGraduationWorkFieldChange.bind(this);
        this.getTeacherApprovals = this.getTeacherApprovals.bind(this);
    }

    getTeacherApprovals() {
        let id = 0;
        return this.state.teacherApprovals.map(r => {
            id++;
            return (<div key={'teacher-graduation-sheet-row-' + id} className={'teacher-graduation-sheet-row'}>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                    <KeyboardDatePicker
                        format="MM/dd/yyyy"
                        margin="normal"
                        key={'teacher-graduation-sheet-row-' + id}
                        name="receivedAt"
                        className={'received-at'}
                        label="Data vizei:"
                        value={r.approvalDate}
                        onChange={(e) => this.handleDateFieldChange(e, 'receivedAt')}
                        KeyboardButtonProps={{
                            'aria-label': 'change date'
                        }}
                    />
                </MuiPickersUtilsProvider>
                <TextField
                    name="treatedProblems"
                    value={r.treatedProblems}
                    onChange={this.props.handleFieldChange}
                    className={'treated-problems'}
                    key={'treated-problems-' + id}
                    label="Capitole/ problemele analizate"
                    multiline
                    rows={4}
                    rowsMax="10"
                    margin="normal"
                    variant="outlined"
                />
            </div>)
        });
    }

    handleGraduationWorkFieldChange(event) {
        const value = event.target.value;
        const {graduationWork} = this.state;
        graduationWork[event.target.name] = value;

        this.setState({
            graduationWork
        });
    }

    render() {
        const graduationWorkDummyData = {
            title: this.state.graduationWork.title,
            treatedProblems: this.state.graduationWork.treatedProblems,
            practicePlaceAndTime: this.state.graduationWork.practicePlaceAndTime,
            bibliography: this.state.graduationWork.bibliography,
            particularAspects: this.state.graduationWork.particularAspects
        };

        return (
            <div>
                <StudentInfo studentInfo={studentInfoDummyData}/>
                <GraduationWork graduationWork={graduationWorkDummyData}
                                handleFieldChange={this.handleGraduationWorkFieldChange}/>

                <Paper className={'date-card'}>
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <KeyboardDatePicker
                            format="MM/dd/yyyy"
                            margin="normal"
                            id="received-at"
                            name="receivedAt"
                            className={'block-element'}
                            label="Primit tema la data de:"
                            value={this.state.receivedAt}
                            onChange={(e) => this.handleDateFieldChange(e, 'receivedAt')}
                            KeyboardButtonProps={{
                                'aria-label': 'change date'
                            }}
                        />
                    </MuiPickersUtilsProvider>
                </Paper>

                <Paper className={'date-card'}>
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <KeyboardDatePicker
                            format="MM/dd/yyyy"
                            margin="normal"
                            id="submission-date"
                            name="submissionDate"
                            className={'block-element'}
                            label="Data predării lucrării:"
                            value={this.state.submissionDate}
                            onChange={(e) => this.handleDateFieldChange(e, 'submissionDate')}
                            KeyboardButtonProps={{
                                'aria-label': 'change date'
                            }}
                        />
                    </MuiPickersUtilsProvider>
                </Paper>

                <Paper className={'card'}>
                    <h2 className={'graduation-work-form-title'}>LUCRARE DE ABSOLVIRE/ LUCRARE DE LICENŢĂ/ PROIECT DE
                        DIPLOMĂ/
                        DISERTAŢIE – VIZE
                    </h2>
                    {this.getTeacherApprovals()}
                </Paper>

                <button className={'save-btn'}>
                    Salveaza modificarile
                </button>
            </div>
        );
    }
}

const studentInfoDummyData = {
    faculty: 'Facultatea de Stiinte Economice si Administrarea Afacerilor',
    department: 'Finanțe, Contabilitate și Teorie Economică',
    studyProgram: 'Contabilitate si informatica de gestiune',
    universityYear: 3,
    candidate: 'Popescu Maria',
    group: '13LF671',
    promotion: '2017-2020',
    guidingTeacher: 'Ionescu Andrei',
    guidingTeacherDepartment: 'Management și Informatică Economică'
};