import React from "react";
import './PreliminarySheet.css';
import Paper from "@material-ui/core/Paper";
import DateFnsUtils from '@date-io/date-fns';
import StudentInfo from "../StudentInfo/StudentInfo";
import {KeyboardDatePicker, MuiPickersUtilsProvider} from '@material-ui/pickers';
import GraduationWork from "../GraduationWork/GraduationWork";

export default class PreliminarySheet extends React.Component {

    constructor(props) {
        super(props);

        const defaultDate = new Date('2020-01-01');

        this.state = {
            mandatoryDeadlineOne: defaultDate,
            mandatoryDeadlineTwo: defaultDate,
            mandatoryDeadlineThree: defaultDate,
            mandatoryDeadlineFour: defaultDate,
            submissionDeadline: defaultDate
        };

        this.handleMandatoryDeadlinesChange = this.handleMandatoryDeadlinesChange.bind(this);
        this.handleSubmissionDeadlineChange = this.handleSubmissionDeadlineChange.bind(this);
    }

    handleMandatoryDeadlinesChange(newDate, mandatoryDeadlinesOrder) {
        switch (mandatoryDeadlinesOrder) {
            case 1:
                this.setState({mandatoryDeadlineOne: newDate});
                break;
            case 2:
                this.setState({mandatoryDeadlineTwo: newDate});
                break;
            case 3:
                this.setState({mandatoryDeadlineThree: newDate});
                break;
            default:
                this.setState({mandatoryDeadlineFour: newDate});
                break;
        }
    };

    handleSubmissionDeadlineChange(date) {
        this.setState({submissionDeadline: date})
    }

    render() {
        const graduationWorkDummyData = {
            title: this.state.title,
            treatedProblems: this.state.treatedProblems,
            practicePlaceAndTime: this.state.practicePlaceAndTime,
            bibliography: this.state.bibliography,
            particularAspects: this.state.particularAspects
        };

        return (
            <div>
                <StudentInfo studentInfo={studentInfoDummyData}/>
                <GraduationWork graduationWork={graduationWorkDummyData} handleFieldChange={this.handleFieldChange}/>

                <Paper className={'card'}>
                    <p>Termene obligatorii de consultaţii:</p>
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <div className={'mandatory-consultation-deadlines-row'}>
                            <KeyboardDatePicker
                                format="MM/dd/yyyy"
                                margin="normal"
                                id="consult-deadline-1"
                                label="Termen 1"
                                value={this.state.mandatoryDeadlineOne}
                                onChange={(e) => this.handleMandatoryDeadlinesChange(e, 1)}
                                KeyboardButtonProps={{
                                    'aria-label': 'change date'
                                }}
                            />
                            <KeyboardDatePicker
                                format="MM/dd/yyyy"
                                margin="normal"
                                id="consult-deadline-2"
                                label="Termen 2"
                                value={this.state.mandatoryDeadlineTwo}
                                onChange={(e) => this.handleMandatoryDeadlinesChange(e, 2)}
                                KeyboardButtonProps={{
                                    'aria-label': 'change date'
                                }}
                            />
                            <KeyboardDatePicker
                                format="MM/dd/yyyy"
                                margin="normal"
                                id="consult-deadline-3"
                                label="Termen 3"
                                value={this.state.mandatoryDeadlineThree}
                                onChange={(e) => this.handleMandatoryDeadlinesChange(e, 3)}
                                KeyboardButtonProps={{
                                    'aria-label': 'change date'
                                }}
                            />
                            <KeyboardDatePicker
                                format="MM/dd/yyyy"
                                margin="normal"
                                id="consult-deadline-4"
                                label="Termen 4"
                                value={this.state.mandatoryDeadlineFour}
                                onChange={(e) => this.handleMandatoryDeadlinesChange(e, 4)}
                                KeyboardButtonProps={{
                                    'aria-label': 'change date'
                                }}
                            />
                        </div>
                        <KeyboardDatePicker
                            format="MM/dd/yyyy"
                            margin="normal"
                            id="submission-date"
                            className={'block-element'}
                            label="Termenul de predare:"
                            value={this.state.submissionDeadline}
                            onChange={this.handleSubmissionDeadlineChange}
                            KeyboardButtonProps={{
                                'aria-label': 'change date'
                            }}
                        />
                    </MuiPickersUtilsProvider>
                </Paper>
                <button className={'save-btn'}>
                    Salveaza modificarile
                </button>
            </div>

        );
    }
}

const studentInfoDummyData = {
    faculty: 'Facultatea de Stiinte Economice si Administrarea Afacerilor',
    department: 'Finanțe, Contabilitate și Teorie Economică',
    studyProgram: 'Contabilitate si informatica de gestiune',
    universityYear: 3,
    candidate: 'Popescu Maria',
    group: '13LF671',
    promotion: '2017-2020',
    guidingTeacher: 'Ionescu Andrei',
    guidingTeacherDepartment: 'Management și Informatică Economică'
};