import React from "react";
import InputLabel from "@material-ui/core/InputLabel";
import './SubjectAndTeacherGroup.css';
import TextField from "@material-ui/core/TextField";
import Select from "react-select";

export default class SubjectAndTeacherGroup extends React.Component {

    constructor(props) {
        super(props);

        this.getSubjectInputValidationError = this.getSubjectInputValidationError.bind(this);
        this.getTeacherInputValidationError = this.getTeacherInputValidationError.bind(this);
        this.getTeacherSelectEventObject = this.getTeacherSelectEventObject.bind(this);
        this.getInputName = this.getInputName.bind(this);
    }

    getSubjectInputValidationError() {
        if (this.props.subjectInputError.length > 0) {
            return <p className={'validation-error'}>{this.props.subjectInputError}</p>;
        }

        return null;
    }

    getTeacherInputValidationError() {
        if (this.props.teacherInputError.length > 0) {
            return <p className={'validation-error'}>{this.props.teacherInputError}</p>;
        }

        return null;
    }

    getInputName(input) {
        return input + this.props.inputNumber;
    }

    getTeacherSelectEventObject(selectedTeacher) {
        const event = {};
        event.target = {};
        event.target.name = this.getInputName(inputName.TEACHER);
        event.target.value = selectedTeacher;

        return event;
    }

    render() {
        return (
            <div className={'form-group'}>
                <TextField
                    id="subject"
                    name={this.getInputName(inputName.SUBJECT)}
                    label={this.props.subjectLabel}
                    placeholder="Numele Temei.."
                    type="search"
                    onChange={this.props.handleFieldChange}
                />
                {this.getSubjectInputValidationError()}

                <InputLabel className={'label'} htmlFor="teacher-select">Profesori</InputLabel>
                <Select
                    id="teacher-select"
                    name={this.getInputName(inputName.TEACHER)}
                    options={this.props.teachers}
                    onChange={selectedTeacher => {
                        this.props.handleFieldChange(this.getTeacherSelectEventObject(selectedTeacher.value))
                    }}
                    placeholder="Cauta un profesor.."
                    noOptionsMessage={() => 'Numele profesorului nu este valid'}
                />
                {this.getTeacherInputValidationError()}

                <TextField
                    id="comments"
                    className={'comments'}
                    name={this.getInputName(inputName.COMMENTS)}
                    onChange={this.props.handleFieldChange}
                    label={'Comentarii'}
                    multiline
                    rows={4}
                    rowsMax="4"
                    margin="normal"
                    variant="outlined"
                />
            </div>
        );
    }
}

const inputName = {
    SUBJECT: 'subject',
    TEACHER: 'teacher',
    COMMENTS: 'comments'
};
