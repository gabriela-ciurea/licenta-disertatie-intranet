import React from 'react';
import './StudentGraduationSubjectForm.css';
import SubjectAndTeacherGroup from "./SubjectAndTeacherGroup/SubjectAndTeacherGroup";
import Paper from "@material-ui/core/Paper";

export default class StudentGraduationSubjectForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            teachers: [],
            fields: {
                subjectOne: '',
                subjectTwo: '',
                subjectThree: '',
                teacherOne: '',
                teacherTwo: '',
                teacherThree: '',
                commentsOne: '',
                commentsTwo: '',
                commentsThree: '',
                selectedTeachers: []
            },
            validationErrors: {
                subjectOne: '',
                subjectTwo: '',
                subjectThree: '',
                teacherOne: '',
                teacherTwo: '',
                teacherThree: ''
            },
            isSubmitSuccessful: false
        };

        this.getFilteredTeachers = this.getFilteredTeachers.bind(this);
        this.updateSelectedTeachersList = this.updateSelectedTeachersList.bind(this);
        this.handleFieldChange = this.handleFieldChange.bind(this);
        this.validateSubject = this.validateSubject.bind(this);
        this.validateTeacher = this.validateTeacher.bind(this);
        this.getSubmitSuccessfulMessage = this.getSubmitSuccessfulMessage.bind(this);
        this.countValidationErrors = this.countValidationErrors.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        fetch('https://restcountries.eu/rest/v2/all')
            .then(response => response.json())
            .then(teachers => {
                teachers = teachers.map(teacher => ({
                    value: teacher.name,
                    label: teacher.name
                }));
                this.setState({teachers: teachers})
            });
    }

    handleFieldChange(event) {
        const {name, value} = event.target;
        const {fields} = this.state;
        fields[name] = value;
        this.setState(fields);

        if (name.includes(InputNameEnum.SUBJECT)) {
            this.validateSubject(name, value);
        } else if (name.includes(InputNameEnum.TEACHER)) {
            this.validateTeacher(name, value);
            this.updateSelectedTeachersList(value);
        }
    }

    getFilteredTeachers() {
        return this.state.teachers.filter(teacher => !this.state.fields.selectedTeachers.includes(teacher.value));
    }

    updateSelectedTeachersList(selectedTeacher) {
        const {fields} = this.state;
        let {selectedTeachers} = fields;

        const teacherFieldValues = [];
        teacherFieldValues.push(fields.teacherOne);
        teacherFieldValues.push(fields.teacherTwo);
        teacherFieldValues.push(fields.teacherThree);

        selectedTeachers.push(selectedTeacher);
        selectedTeachers = selectedTeachers.filter(st =>
            teacherFieldValues.includes(st)
        );

        fields.selectedTeachers = selectedTeachers;

        this.setState(fields);
    }

    validateSubject(name, value) {
        const errors = this.state.validationErrors;

        if (value.length < 10) {
            errors[name] = 'Numele temei trebuie sa contina minim 10 caractere.';
        } else if (value.length > 100) {
            errors[name] = 'Numele temei trebuie sa contina maxim 100 caractere.';
        } else {
            errors[name] = '';
        }

        this.setState({validationErrors: errors});
    }

    validateTeacher(name, value) {
        const errors = this.state.validationErrors;

        if (value.length < 1) {
            errors[name] = 'Alegeti un profesor.';
        } else {
            errors[name] = '';
        }

        this.setState({validationErrors: errors})
    }

    countValidationErrors() {
        let errorsCount = 0;

        Object.keys(this.state.validationErrors).forEach(
            (key) => {
                if (this.state.validationErrors[key].length > 0) {
                    errorsCount++;
                }
            }
        );

        return errorsCount;
    }

    handleSubmit(event) {
        event.preventDefault();
        const {fields} = this.state;

        Object.keys(fields).forEach(
            (key) => {
                if (key.includes('subject')) {
                    this.validateSubject(key, fields[key]);
                } else if (key.includes('teacher')) {
                    this.validateTeacher(key, fields[key]);
                }
            }
        );

        if (this.countValidationErrors() === 0) {
            this.setState({isSubmitSuccessful: true});
        }
    }

    getSubmitSuccessfulMessage() {
        if (this.state.isSubmitSuccessful) {
            return <p className={'success-msg'}>&#10003; Cererea ta a fost trimisa cu succes!</p>
        }

        return null;
    }

    render() {
        return (
            <Paper>
                <form onSubmit={this.handleSubmit} className={'student-graduation-subject-form'}>
                    <SubjectAndTeacherGroup inputNumber={InputNumberEnum.ONE}
                                            teachers={this.getFilteredTeachers()}
                                            subjectLabel={SubjectLabelEnum.SUBJECT_ONE}
                                            handleFieldChange={this.handleFieldChange}
                                            subjectInputError={this.state.validationErrors.subjectOne}
                                            teacherInputError={this.state.validationErrors.teacherOne}/>
                    <p>În cazul în care nu voi fi acceptat, următoarele opţiuni sunt: </p>
                    <SubjectAndTeacherGroup inputNumber={InputNumberEnum.TWO}
                                            teachers={this.getFilteredTeachers()}
                                            subjectLabel={SubjectLabelEnum.SUBJECT_TWO}
                                            handleFieldChange={this.handleFieldChange}
                                            subjectInputError={this.state.validationErrors.subjectTwo}
                                            teacherInputError={this.state.validationErrors.teacherTwo}/>
                    <SubjectAndTeacherGroup inputNumber={InputNumberEnum.THREE}
                                            teachers={this.getFilteredTeachers()}
                                            subjectLabel={SubjectLabelEnum.SUBJECT_THREE}
                                            handleFieldChange={this.handleFieldChange}
                                            subjectInputError={this.state.validationErrors.subjectThree}
                                            teacherInputError={this.state.validationErrors.teacherThree}/>

                    <button className={'submit-btn'}>
                        Trimiteti
                    </button>

                    {this.getSubmitSuccessfulMessage()}
                </form>
            </Paper>
        );
    }
}

const InputNameEnum = {
    SUBJECT: 'subject',
    TEACHER: 'teacher',
    COMMENTS: 'comments'
};

const InputNumberEnum = {
    ONE: 'One',
    TWO: 'Two',
    THREE: 'Three'
};

const SubjectLabelEnum = {
    SUBJECT_ONE: '1. Tema (principala)',
    SUBJECT_TWO: '2. Tema',
    SUBJECT_THREE: '3. Tema'
};
