import React from "react";
import {withStyles} from '@material-ui/core/styles';
import {green, red} from '@material-ui/core/colors';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
import './TeacherChooseStudentTable.css';
import TablePagination from "@material-ui/core/TablePagination";
import TableFooter from "@material-ui/core/TableFooter";

export default class TeacherChooseStudentTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            rows: this.disableRadioButtons(rows),
            rowsPerPage: 5,
            page: 0
        };

        this.handleApplicationStatusChange = this.handleApplicationStatusChange.bind(this);
        this.handleChangePage = this.handleChangePage.bind(this);
        this.handleChangeRowsPerPage = this.handleChangeRowsPerPage.bind(this);
        this.disableRadioButtons = this.disableRadioButtons.bind(this);
    }

    handleApplicationStatusChange(studentId, event) {
        const {rows} = this.state;
        rows.forEach(r => {
            if (r.id === studentId) {
                r.applicationStatus = event.target.value;
            }
        });
        this.setState({rows: rows})
    }

    disableRadioButtons(rows) {
        return rows.map(row => {
            row.isDisabled = row.applicationStatus !== ApplicationStatusEnum.UNSELECTED;
            return row;
        });
    }

    render() {
        const {rows} = this.state;
        const {rowsPerPage} = this.state;
        const {page} = this.state;

        return (
            <div>
                <Paper className={'paper'}>
                    <Table aria-label="simple table">
                        <TableHead className={'table-head'}>
                            <TableRow>
                                <TableCell className={'table-head-row'}>Nr.</TableCell>
                                <TableCell className={'table-head-row'}>Nume Student</TableCell>
                                <TableCell className={'table-head-row'}>Nume Tema</TableCell>
                                <TableCell className={'table-head-row'}>Comentarii</TableCell>
                                <TableCell className={'table-head-row'}>Statusul Cererii</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row, index) => (
                                <TableRow key={row.id}>
                                    <TableCell>{index + 1 + page * rowsPerPage}</TableCell>
                                    <TableCell>{row.studentName}</TableCell>
                                    <TableCell>{row.subjectName}</TableCell>
                                    <TableCell className={'comments'}>{row.comments}</TableCell>
                                    <TableCell>
                                        <RadioGroup aria-label="application-status" name="applicationStatus"
                                                    value={row.applicationStatus}
                                                    onChange={e => this.handleApplicationStatusChange(row.id, e)}>
                                            <FormControlLabel className={'unselected'}
                                                              value={ApplicationStatusEnum.UNSELECTED}
                                                              control={<Radio color="default"
                                                                              disabled={row.isDisabled}/>}
                                                              label="Neselectat"/>
                                            <FormControlLabel value={ApplicationStatusEnum.ACCEPTED}
                                                              control={<GreenRadio
                                                                  disabled={row.isDisabled}/>}
                                                              label="Acceptat"/>
                                            <FormControlLabel value={ApplicationStatusEnum.REJECTED}
                                                              control={<RedRadio
                                                                  disabled={row.isDisabled}/>}
                                                              label="Respins"/>
                                        </RadioGroup>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                        <TableFooter>
                            <TableRow>
                                <TablePagination
                                    rowsPerPageOptions={[5, 10, 25]}
                                    labelRowsPerPage={'Nr. randuri pe pagina'}
                                    count={rows.length}
                                    rowsPerPage={rowsPerPage}
                                    page={page}
                                    backIconButtonProps={{
                                        'aria-label': 'previous page',
                                    }}
                                    nextIconButtonProps={{
                                        'aria-label': 'next page',
                                    }}
                                    onChangePage={this.handleChangePage}
                                    onChangeRowsPerPage={this.handleChangeRowsPerPage}
                                />
                            </TableRow>
                        </TableFooter>
                    </Table>
                </Paper>
                <button className={'save-btn'}>
                    Salveaza modificarile
                </button>
            </div>
        );
    }

    handleChangePage(event, newPage) {
        this.setState({page: newPage})
    };

    handleChangeRowsPerPage(event) {
        const rowsPerPage = parseInt(event.target.value, 10);
        if (rowsPerPage > this.state.rows.length) {
            this.setState({page: 0});
            this.setState({rowsPerPage: rowsPerPage});
        } else {
            this.setState({rowsPerPage: rowsPerPage});
        }
    };
}

const ApplicationStatusEnum = {
    UNSELECTED: 'UNSELECTED',
    ACCEPTED: 'ACCEPTED',
    REJECTED: 'REJECTED'
};

const rows = [
    {
        id: 1,
        studentName: 'Nume Premune 1',
        subjectName: 'Nume subiect 1',
        comments: 'Nici un comentariu',
        applicationStatus: ApplicationStatusEnum.UNSELECTED
    },
    {
        id: 2,
        studentName: 'Nume Premune 2',
        subjectName: 'Nume subiect 2',
        comments: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        applicationStatus: ApplicationStatusEnum.ACCEPTED
    },
    {
        id: 3,
        studentName: 'Nume Premune 3',
        subjectName: 'Nume subiect 3',
        comments: 'Nici un comentariu',
        applicationStatus: ApplicationStatusEnum.REJECTED
    },
    {
        id: 4,
        studentName: 'Nume Premune 4',
        subjectName: 'Nume subiect 3',
        comments: 'Nici un comentariu',
        applicationStatus: ApplicationStatusEnum.ACCEPTED
    },
    {
        id: 5,
        studentName: 'Nume Premune 5',
        subjectName: 'Nume subiect 3',
        comments: 'Nici un comentariu',
        applicationStatus: ApplicationStatusEnum.REJECTED
    },
    {
        id: 6,
        studentName: 'Nume Premune 6',
        subjectName: 'Nume subiect 3',
        comments: 'Nici un comentariu',
        applicationStatus: ApplicationStatusEnum.ACCEPTED
    },
    {
        id: 7,
        studentName: 'Nume Premune 7',
        subjectName: 'Nume subiect 3',
        comments: 'Nici un comentariu',
        applicationStatus: ApplicationStatusEnum.REJECTED
    }
];

const GreenRadio = withStyles({
    root: {
        color: green[400],
        '&$checked': {
            color: green[600],
        },
    },
    checked: {},
})(props => <Radio color="default" {...props} />);

const RedRadio = withStyles({
    root: {
        color: red[400],
        '&$checked': {
            color: red[600],
        },
    },
    checked: {},
})(props => <Radio color="default" {...props} />);