import React from "react";
import './GraduationSheet.css';
import StudentInfo from "../StudentInfo/StudentInfo";
import {KeyboardDatePicker, MuiPickersUtilsProvider} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import Paper from "@material-ui/core/Paper";
import GraduationWork from "../GraduationWork/GraduationWork";

export default class GraduationSheet extends React.Component {

    constructor(props) {
        super(props);

        const defaultDate = new Date('2020-01-01');

        this.state = {
            title: '',
            treatedProblems: '',
            practicePlaceAndTime: '',
            bibliography: '',
            particularAspects: '',
            receivedAt: defaultDate,
            submissionDate: defaultDate,
        };

        this.handleFieldChange = this.handleFieldChange.bind(this);
        this.handleDateFieldChange = this.handleDateFieldChange.bind(this);
    }

    handleFieldChange(event) {
        const value = event.target.value;
        this.setState({
            [event.target.name]: value
        });
    }

    handleDateFieldChange(newDate, name) {
        this.setState({
            [name]: newDate
        });
    }

    render() {
        const graduationWorkDummyData = {
            title: this.state.title,
            treatedProblems: this.state.treatedProblems,
            practicePlaceAndTime: this.state.practicePlaceAndTime,
            bibliography: this.state.bibliography,
            particularAspects: this.state.particularAspects
        };

        return (
            <div>
                <StudentInfo studentInfo={studentInfoDummyData}/>
                <GraduationWork graduationWork={graduationWorkDummyData} handleFieldChange={this.handleFieldChange}/>

                <Paper className={'date-card'}>
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <KeyboardDatePicker
                            format="MM/dd/yyyy"
                            margin="normal"
                            id="received-at"
                            name="receivedAt"
                            className={'block-element'}
                            label="Primit tema la data de:"
                            value={this.state.receivedAt}
                            onChange={(e) => this.handleDateFieldChange(e, 'receivedAt')}
                            KeyboardButtonProps={{
                                'aria-label': 'change date'
                            }}
                        />
                    </MuiPickersUtilsProvider>
                </Paper>

                <Paper className={'date-card'}>
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <KeyboardDatePicker
                            format="MM/dd/yyyy"
                            margin="normal"
                            id="submission-date"
                            name="submissionDate"
                            className={'block-element'}
                            label="Data predării lucrării:"
                            value={this.state.submissionDate}
                            onChange={(e) => this.handleDateFieldChange(e, 'submissionDate')}
                            KeyboardButtonProps={{
                                'aria-label': 'change date'
                            }}
                        />
                    </MuiPickersUtilsProvider>
                </Paper>
                <button className={'save-btn'}>
                    Salveaza modificarile
                </button>
            </div>
        );
    }
}

const studentInfoDummyData = {
    faculty: 'Facultatea de Stiinte Economice si Administrarea Afacerilor',
    department: 'Finanțe, Contabilitate și Teorie Economică',
    studyProgram: 'Contabilitate si informatica de gestiune',
    universityYear: 3,
    candidate: 'Popescu Maria',
    group: '13LF671',
    promotion: '2017-2020',
    guidingTeacher: 'Ionescu Andrei',
    guidingTeacherDepartment: 'Management și Informatică Economică'
};
