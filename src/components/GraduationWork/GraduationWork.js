import React from "react";
import TextField from "@material-ui/core/TextField";
import Paper from "@material-ui/core/Paper";
import './GraduationWork.css';

export default class GraduationWork extends React.Component {

    render() {
        const {graduationWork} = this.props;

        return (
            <Paper className={'card'}>
                <form onSubmit={this.handleSubmit} className={'graduation-sheet-form'}>
                    <h2 className={'graduation-work-form-title'}>LUCRARE DE ABSOLVIRE/ LUCRARE DE LICENŢĂ/ PROIECT
                        DE DIPLOMĂ/DISERTAŢIE
                    </h2>

                    <div className={'graduation-work-form-field'}>
                        <TextField
                            id="subject"
                            name="title"
                            value={graduationWork.title}
                            label="Titlul lucrării"
                            type="search"
                            onChange={this.props.handleFieldChange}
                        />
                        {/*{this.getInputValidationError()}*/}
                    </div>

                    <div className={'graduation-work-form-field'}>
                        <TextField
                            id="treated-problems"
                            name="treatedProblems"
                            value={graduationWork.treatedProblems}
                            onChange={this.props.handleFieldChange}
                            label={'Problemele principale tratate'}
                            multiline
                            rows={4}
                            rowsMax="10"
                            margin="normal"
                            variant="outlined"
                        />
                        {/*{this.getInputValidationError()}*/}
                    </div>

                    <div className={'graduation-work-form-field'}>
                        <TextField
                            id="practicePlaceAndTime"
                            name="practicePlaceAndTime"
                            value={graduationWork.practicePlaceAndTime}
                            label="Locul şi durata practicii"
                            type="search"
                            onChange={this.props.handleFieldChange}
                        />
                    </div>

                    <div className={'graduation-work-form-field'}>
                        <TextField
                            id="bibliography"
                            name="bibliography"
                            value={graduationWork.bibliography}
                            onChange={this.props.handleFieldChange}
                            label={'Bibliografie'}
                            multiline
                            rows={4}
                            rowsMax="10"
                            margin="normal"
                            variant="outlined"
                        />
                    </div>

                    <div className={'graduation-work-form-field'}>
                        <TextField
                            id="particular-aspects"
                            name="particularAspects"
                            value={graduationWork.particularAspects}
                            onChange={this.props.handleFieldChange}
                            label={'Aspecte particulare'}
                            placeholder={'(desene obligatorii, aplicaţii practice, metode specifice recomandate)'}
                            multiline
                            rows={4}
                            rowsMax="10"
                            margin="normal"
                            variant="outlined"
                        />
                    </div>
                </form>
            </Paper>
        );
    }
}