import React from "react";
import './StudentInfo.css';
import Paper from "@material-ui/core/Paper";

export default class StudentInfo extends React.Component {

    render() {
        const {studentInfo} = this.props;

        return (
            <Paper className={'card'}>
                <h2>Universitatea Transilvania din Braşov</h2>
                <div className={'student-info-row'}>
                    <div className={'student-info-column'}>
                        <p className={'student-info-column-title'}>Facultatea:</p>
                        <p>{studentInfo.faculty}</p>
                    </div>
                    <div className={'student-info-column'}>
                        <p className={'student-info-column-title'}>Departamentul:</p>
                        <p>{studentInfo.department}</p>
                    </div>
                </div>

                <div className={'student-info-row'}>
                    <div className={'student-info-column'}>
                        <p className={'student-info-column-title'}>Programul de studii:</p>
                        <p>{studentInfo.studyProgram}</p>
                    </div>
                    <div className={'student-info-column'}>
                        <p className={'student-info-column-title'}>Anul universitar:</p>
                        <p>{studentInfo.universityYear}</p>
                    </div>
                </div>

                <div className={'student-info-row'}>
                    <div className={'student-info-column'}>
                        <p className={'student-info-column-title'}>Candidat:</p>
                        <p>{studentInfo.candidate}</p>
                    </div>
                    <div className={'student-info-grouped-columns'}>
                        <div className={'student-info-column'}>
                            <p className={'student-info-column-title'}>Grupa:</p>
                            <p>{studentInfo.group}</p>
                        </div>
                        <div className={'student-info-column'}>
                            <p className={'student-info-column-title'}>Promotia:</p>
                            <p>{studentInfo.promotion}</p>
                        </div>
                    </div>
                </div>

                <div className={'student-info-row'}>
                    <div className={'student-info-column'}>
                        <p className={'student-info-column-title'}>Cadrul didactic indrumator:</p>
                        <p>{studentInfo.guidingTeacher}</p>
                    </div>
                    <div className={'student-info-column'}>
                        <p className={'student-info-column-title'}>Departament:</p>
                        <p>{studentInfo.guidingTeacherDepartment}</p>
                    </div>
                </div>
            </Paper>);
    }
}