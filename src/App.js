import React from 'react';
import './App.css';
import StudentGraduationSubjectForm from "./components/StudentGraduationSubjectForm/StudentGraduationSubjectForm";
import TeacherChooseStudentTable from "./components/TeacherChooseStudentTable/TeacherChooseStudentTable";
import PreliminarySheet from "./components/PreliminarySheet/PreliminarySheet";
import GraduationSheet from "./components/GraduationSheet/GraduationSheet";
import {BrowserRouter as Router, Route, Link, Switch} from "react-router-dom";
import TeacherGraduationSheet from "./components/TeacherGraduationSheet/TeacherGraduationSheet";

function App() {
    return (
        <div className="app">
            <Router>
                <nav>
                    <ul className={'nav-bar'}>
                        <li>
                            <Link className={'link'} to="/">1. Tema Lucrare</Link>
                        </li>
                        <li>
                            <Link className={'link'} to="/choose-student-table">2. Alege Student</Link>
                        </li>
                        <li>
                            <Link className={'link'} to="/preliminary-sheet">3. Fisa Preliminara</Link>
                        </li>
                        <li>
                            <Link className={'link'} to="/graduation-sheet">4. Fisa Absolvire</Link>
                        </li>
                        <li>
                            <Link className={'link'} to="/teacher-graduation-sheet">5. Fisa Absolvire Profesor</Link>
                        </li>
                    </ul>
                </nav>

                <Switch>
                    <Route exact path="/">
                        <StudentGraduationSubjectForm/>
                    </Route>
                    <Route exact path="/choose-student-table">
                        <TeacherChooseStudentTable/>
                    </Route>
                    <Route exact  path="/preliminary-sheet">
                        <PreliminarySheet/>
                    </Route>
                    <Route exact  path="/graduation-sheet">
                        <GraduationSheet/>
                    </Route>
                    <Route exact  path="/teacher-graduation-sheet">
                        <TeacherGraduationSheet/>
                    </Route>
                </Switch>
            </Router>
        </div>
    );
}

export default App;
